# JAPAN TRAVEL BLOG 2019 :sushi:

A Travel Blog about Japan, visited March 2019 - April 2019. All the images displayed of the japanese cities are property of the author of this website, Kerstin Bonev-Wagner.


## URL

Please visit the travel blog [here](http://kwagner91.gitlab.io/japan/)


## Built With

* [HTML5](https://www.oracle.com/technetwork/java/javase/downloads/index.html) - The Programming Language used
* [CSS3](https://openjfx.io/) - Creating Java applications with a modern, hardware-accelerated user interface
* [Git](https://git-scm.com/) - Open Source Version Control System
* [Bulma](https://bulma.io/) - Free and open source CSS framework based on Flexbox


## Authors

* **Kerstin Bonev-Wagner** - *Initial work* - [Gitlab](https://gitlab.com/KWagner91)


## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].


