// Navbar Hamburger Navigation
document.addEventListener('DOMContentLoaded', () => {
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  if ($navbarBurgers.length > 0) {
    $navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {
        const target = el.dataset.target;
        const $target = document.getElementById(target);
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');
      });
    });
  }

});

// Google Map API
function initMap() {
    const myLatLng = {lat: 35.652832, lng: 139.839478};
    
    // Load Map
    let map = new google.maps.Map(document.getElementById('google-map'), {
      zoom: 6,
      center: myLatLng
    });

    // Load Data Points of Cities
    let himeji = new google.maps.Marker({
        position: {lat: 34.8154, lng: 134.6856},
        map: map,
        title: 'Himeji'
    })

    let hiroshima = new google.maps.Marker({
        position: {lat: 34.383331, lng: 132.449997},
        map: map,
        title: 'Hiroshima'
    })

    let isawa = new google.maps.Marker({
        position: {lat: 35.6577, lng: 138.6353},
        map: map,
        title: 'Isawa'
    })

    let kamakura = new google.maps.Marker({
        position: {lat: 35.3192, lng: 139.5467},
        map: map,
        title: 'Kamakura'
    })

    let kanazawa = new google.maps.Marker({
        position: {lat: 36.5610, lng: 136.6566},
        map: map,
        title: 'Kanazawa'
    })

    let koya = new google.maps.Marker({
        position: {lat: 34.217, lng: 135.583},
        map: map,
        title: 'Koya-san'
    })

    let kyoto = new google.maps.Marker({
        position: {lat: 35.011665, lng: 135.768326},
        map: map,
        title: 'Kyoto'
    })

    let matsumoto = new google.maps.Marker({
        position: {lat: 36.237148, lng:	137.964951},
        map: map,
        title: 'Matsumoto'
    })

    let nagasaki = new google.maps.Marker({
        position: {lat: 32.764233, lng: 129.872696},
        map: map,
        title: 'Nagasaki'
    })

    let nara = new google.maps.Marker({
        position: {lat: 34.6851, lng: 135.8048},
        map: map,
        title: 'Nara'
    })

    let nikko = new google.maps.Marker({
        position: {lat: 36.7199, lng: 139.6982},
        map: map,
        title: 'Nikko'
    })

    let osaka = new google.maps.Marker({
        position: {lat: 34.669529, lng:	135.497009},
        map: map,
        title: 'Osaka'
    })

    let shirakawago = new google.maps.Marker({
        position: {lat: 36.2710, lng: 136.8986},
        map: map,
        title: 'Shirakawago'
    })

    let takamatsu = new google.maps.Marker({
        position: {lat: 34.3425, lng: 134.0465},
        map: map,
        title: 'Takamatsu'
    })

    let takayama = new google.maps.Marker({
        position: {lat: 36.1461, lng: 137.2522},
        map: map,
        title: 'Takayama'
    })

    let tokyo = new google.maps.Marker({
        position: {lat: 35.652832, lng: 139.839478},
        map: map,
        title: 'Tokyo'
      });
   
  }


  